
const chai = require('chai');
const expect = chai.expect;

const Mongoose = require('mongoose');

const {User} = require('../build/persistance/User');

describe('User model tests', function() {

    before(async function(){

        await Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });
    });

    beforeEach(async function() {

        await User.deleteMany({});
    });

    describe('Full name virtual attribute tests', function() {

        it('Should allow setting a full name virtual', async function() {

            // Instantiate a new user (don't save yet!)
            const user = await new User({ firstName: 'FIRST_NAME', lastName: 'LAST_NAME' });

            // User should have the initial field values provided above
            expect(user.firstName).to.equal('FIRST_NAME');
            expect(user.lastName).to.equal('LAST_NAME');

            // Set the fullName virtual which should change the first and last name
            user.fullName = 'Hubert-Farnsworth';

            // Ensure the virtual attribute did its job setting the first and last name
            expect(user.firstName).to.equal('Hubert');
            expect(user.lastName).to.equal('Farnsworth');

            // Save the user
            const savedUser = await user.save();

            // Ensure that by the modified first and last name values were persisted
            expect(savedUser.firstName).to.equal('Hubert');
            expect(savedUser.lastName).to.equal('Farnsworth');
        });
    });

    describe('First name validation tests', function() {

        it('Should prevent setting a non-alpha first name', async function() {

            // Instantiate a new user with a bad first name (don't save yet!)
            const user = await new User({ firstName: '12345', lastName: 'LAST_NAME' });

            try {

                // Saving the user should throw an exception
                await user.save();

                // Minor gotcha, if no errors are thrown the test will pass.
                // If user.save() doesn't throw an error, hard fail the test
                expect.fail("Expected error not thrown");

            } catch (error) {

                // Ensure the expected error was thrown
                expect(error.message).to.equal('user validation failed: firstName: First name may only contain letters')
            }
        });

        it('Should prevent setting john as a first name', async function() {

            // Instantiate a new user with john as a first name (don't save yet!)
            const user = await new User({ firstName: 'john', lastName: 'LAST_NAME' });

            try {

                // Saving the user should throw an exception
                await user.save();

                // Minor gotcha, if no errors are thrown the test will pass.
                // If user.save() doesn't throw an error, hard fail the test
                expect.fail("Expected error not thrown");

            } catch (error) {

                // Ensure the expected error was thrown
                expect(error.message).to.equal('Made up names are not allowed')
            }
        });
    });
});
