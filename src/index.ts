import Mongoose from "mongoose";

import {User} from "./persistance/User";

// Playground for interfacing with Mongoose
(async () => {

    await Mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });

    const user = await new User({ firstName: 'BOB', lastName: 'JDKSJ'});
    user.fullName = "JOHNNY-BRAVO"

    const savedUser = await user.save();
    console.log(savedUser);

    process.exit(0);
})();