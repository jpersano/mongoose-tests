"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
/**
 * User schema with custom validations.
 */
var userSchema = new mongoose_1.default.Schema({
    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 0 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'First name may only contain letters'
        }
    },
    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                return value.length > 2;
            },
            message: 'Last name name must have more than two characters'
        }
    },
    studentId: {
        type: Number,
        validate: {
            validator: function (value) {
                return value > 0;
            },
            message: 'Student ID cannot be negative'
        }
    }
});
/**
 * Prior to validation, check and see if the first name is 'john'.
 * If so, promptly throw an error which will prevent the model from saving if this middleware is called.
 *
 * Note: In this instance, it is better to do this in the validation of firstName itself.
 *       This exists here purely for example.
 */
userSchema.pre('validate', function (next) {
    // Check if the first name is 'john'
    if (this.firstName.toLowerCase() === 'john') {
        next(new Error('Made up names are not allowed'));
    }
    next();
});
/**
 * Don't save a full name to the database as it is a duplication of data we can assemble.
 * Instead, use a getter and setter for a virtual attribute fullName which will handle this on the fly.
 *
 * Note: TypeScript wants 'this' to be typed. The keyword 'this' is special in TypeScript
 *       function parameters and is not actually a parameter in the conventional sense.
 */
userSchema.virtual('fullName')
    .get(function () {
    // Assemble the full name from the first and last names
    return this.firstName + ' ' + this.lastName;
}).set(function (fullName) {
    // For a simple example, the full name must separate the first name and last name with a hyphen
    // If the full name doesn't have a hyphen, throw an error
    if (!fullName.includes('-')) {
        // A proper full name for us would be 'Phillip-Fry'
        throw new Error('Full name must have a hyphen between the first and last name');
    }
    // Split should return two strings in an array, destructure assign them
    // Note: Logic here is brittle, if there are multiple dashes this will get thrown off
    var _a = fullName.split('-'), firstName = _a[0], lastName = _a[1];
    // Set the first name and last name based on pulling apart the full name
    this.firstName = firstName;
    this.lastName = lastName;
});
// Export the compiled model
exports.User = mongoose_1.default.model('user', userSchema);
